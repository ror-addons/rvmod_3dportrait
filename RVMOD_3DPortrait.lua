RVMOD_3DPortrait							= {}
local RVMOD_3DPortrait						= RVMOD_3DPortrait

--GameData.Player.isChicken

local RVName								= "3D Portrait"
local RVCredits								= nil
local RVLicense								= "MIT License"
local RVProjectURL							= "http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534"
local RVRecentUpdates						= 
"09.07.2015 - v1.13 Release\n"..
"\t- Project official site location has been changed\n"..
"\n"..
"21.11.2010 - v1.12 Release\n"..
"\t- Some code clearance been made\n"..
"\t- This release adapted to work in the 1.4.0 game version\n"..
"\n"..
"24.02.2010 - v1.11 Release\n".. 
"\t- Code clearance\n"..
"\t- Adapted to work with the RV Mods Manager v0.99"

local Window3DPortraitSettings				= "RVMOD_3DPortraitSettingsWindow"
local Window3DPortraitBackground			= "PlayerWindow3DPortraitBackground"
local Window3DPortraitFrame					= "PlayerWindow3DPortraitFrame"
local Window3DPortrait						= "PlayerWindow3DPortrait"
local Window3DPortraitParent				= "PlayerWindow"
local Window3DPortraitContainerTemplate		= "3DPortraitContainerTemplate"

local hookCharacterWindowOnShown
local hookCharacterWindowOnHidden
local PortraitsList							= {}

RVMOD_3DPortrait.Templates					= {

	[GameData.CareerLine.IRON_BREAKER]		= {
		[GameData.Gender.MALE]				= "3DPortrait_IRON_BREAKER_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_IRON_BREAKER_FEMALE", -- tested
	},

	[GameData.CareerLine.RUNE_PRIEST]		= {
		[GameData.Gender.MALE]				= "3DPortrait_RUNE_PRIEST_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_RUNE_PRIEST_FEMALE", -- tested
	},

	[GameData.CareerLine.ENGINEER]			= {
		[GameData.Gender.MALE]				= "3DPortrait_ENGINEER_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_ENGINEER_FEMALE",
	},

	[GameData.CareerLine.SLAYER]			= {
		[GameData.Gender.MALE]				= "3DPortrait_SLAYER_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_SLAYER_FEMALE", -- does not exist
	},

	[GameData.CareerLine.WITCH_HUNTER]		= {
		[GameData.Gender.MALE]				= "3DPortrait_WITCH_HUNTER_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_WITCH_HUNTER_FEMALE", -- tested
	},

	[GameData.CareerLine.KNIGHT]			= {
		[GameData.Gender.MALE]				= "3DPortrait_KNIGHT_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_KNIGHT_FEMALE", -- tested
	},

	[GameData.CareerLine.BRIGHT_WIZARD]		= {
		[GameData.Gender.MALE]				= "3DPortrait_BRIGHT_WIZARD_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_BRIGHT_WIZARD_FEMALE", -- tested
	},

	[GameData.CareerLine.WARRIOR_PRIEST]	= {
		[GameData.Gender.MALE]				= "3DPortrait_WARRIOR_PRIEST_MALE", -- tested, need to be tested with a book weapon set
		[GameData.Gender.FEMALE]			= "3DPortrait_WARRIOR_PRIEST_FEMALE", -- tested, need to be tested with a book weapon set
	},

	[GameData.CareerLine.SWORDMASTER]		= {
		[GameData.Gender.MALE]				= "3DPortrait_SWORDMASTER_MALE", -- tested, have some issues with a weapon sets
		[GameData.Gender.FEMALE]			= "3DPortrait_SWORDMASTER_FEMALE", -- tested, have some issues with a weapon sets
	},

	[GameData.CareerLine.SHADOW_WARRIOR]	= {
		[GameData.Gender.MALE]				= "3DPortrait_SHADOW_WARRIOR_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_SHADOW_WARRIOR_FEMALE", -- tested
	},

	[GameData.CareerLine.WHITE_LION]		= {
		[GameData.Gender.MALE]				= "3DPortrait_WHITE_LION_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_WHITE_LION_FEMALE", -- tested
	},

	[GameData.CareerLine.ARCHMAGE]			= {
		[GameData.Gender.MALE]				= "3DPortrait_ARCHMAGE_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_ARCHMAGE_FEMALE", -- tested
	},

	[GameData.CareerLine.BLACK_ORC]			= {
		[GameData.Gender.MALE]				= "3DPortrait_BLACK_ORC_MALE", -- tested, have some issues with the different sets of weapons
		[GameData.Gender.FEMALE]			= "3DPortrait_BLACK_ORC_FEMALE", -- does not exist
	},

	[GameData.CareerLine.SHAMAN]			= {
		[GameData.Gender.MALE]				= "3DPortrait_SHAMAN_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_SHAMAN_FEMALE", -- does not exist
	},

	[GameData.CareerLine.SQUIG_HERDER]		= {
		[GameData.Gender.MALE]				= "3DPortrait_SQUIG_HERDER_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_SQUIG_HERDER_FEMALE", -- does not exist
	},

	[GameData.CareerLine.CHOPPA]			= {
		[GameData.Gender.MALE]				= "3DPortrait_CHOPPA_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_CHOPPA_FEMALE", -- does not exist
	},

	[GameData.CareerLine.CHOSEN]			= {
		[GameData.Gender.MALE]				= "3DPortrait_CHOSEN_MALE", -- tested (have some issues)
		[GameData.Gender.FEMALE]			= "3DPortrait_CHOSEN_FEMALE", -- does not exist
	},

	[GameData.CareerLine.MARAUDER]			= {
		[GameData.Gender.MALE]				= "3DPortrait_MARAUDER_MALE", -- tested (have some issues)
		[GameData.Gender.FEMALE]			= "3DPortrait_MARAUDER_FEMALE", -- does not exist
	},

	[GameData.CareerLine.ZEALOT]			= {
		[GameData.Gender.MALE]				= "3DPortrait_ZEALOT_MALE", -- tested (what a mess!!!)
		[GameData.Gender.FEMALE]			= "3DPortrait_ZEALOT_FEMALE", -- tested
	},

	[GameData.CareerLine.MAGUS]				= {
		[GameData.Gender.MALE]				= "3DPortrait_MAGUS_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_MAGUS_FEMALE", -- tested
	},

	[GameData.CareerLine.BLACKGUARD]		= {
		[GameData.Gender.MALE]				= "3DPortrait_BLACKGUARD_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_BLACKGUARD_FEMALE", -- tested
	},

	[GameData.CareerLine.WITCH_ELF]			= {
		[GameData.Gender.MALE]				= "3DPortrait_WITCH_ELF_MALE", -- does not exist
		[GameData.Gender.FEMALE]			= "3DPortrait_WITCH_ELF_FEMALE", -- tested
	},

	[GameData.CareerLine.DISCIPLE]			= {
		[GameData.Gender.MALE]				= "3DPortrait_DISCIPLE_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_DISCIPLE_FEMALE", -- tested
	},

	[GameData.CareerLine.SORCERER]			= {
		[GameData.Gender.MALE]				= "3DPortrait_SORCERER_MALE", -- tested
		[GameData.Gender.FEMALE]			= "3DPortrait_SORCERER_FEMALE", -- tested
	},
}

--------------------------------------------------------------
-- var DefaultConfiguration
-- Description: default module configuration
--------------------------------------------------------------
RVMOD_3DPortrait.DefaultConfiguration =
{
	Enabled	= true,
}

--------------------------------------------------------------
-- var CurrentConfiguration
-- Description: current module configuration
--------------------------------------------------------------
RVMOD_3DPortrait.CurrentConfiguration =
{
	-- should stay empty, will load in the InitializeConfiguration() function
}

--------------------------------------------------------------
-- function Initialize()
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.Initialize()

	-- First step: load configuration
	RVMOD_3DPortrait.InitializeConfiguration()

	-- Second step: setup CharacterWindow hooks
	hookCharacterWindowOnShown	= CharacterWindow.OnShown
	hookCharacterWindowOnHidden	= CharacterWindow.OnHidden
	CharacterWindow.OnShown		= RVMOD_3DPortrait.OnShownCharacterWindow
	CharacterWindow.OnHidden	= RVMOD_3DPortrait.OnHiddenCharacterWindow

	-- Third step: create 3D portrait for the native PlayerWindow
	CreateWindow(Window3DPortraitBackground, true)
	WindowSetParent(Window3DPortraitBackground, Window3DPortraitParent)
	CreateWindow(Window3DPortraitFrame, true)
	WindowSetParent(Window3DPortraitFrame, Window3DPortraitParent)
	RVMOD_3DPortrait.API_Create3DPortrait(Window3DPortrait, Window3DPortraitParent)
	WindowAddAnchor(Window3DPortrait, "center", Window3DPortraitFrame, "center", 0, 0)
	WindowSetDimensions(Window3DPortrait, 91, 91)

	-- Fourth step: register additional event handlers
	WindowRegisterCoreEventHandler(Window3DPortrait, "OnMouseOver", "PlayerWindow.MouseOverPortrait")
	WindowRegisterCoreEventHandler(Window3DPortrait, "OnMouseOverEnd", "PlayerWindow.MouseOverPortraitEnd")
	WindowRegisterCoreEventHandler(Window3DPortrait, "OnLButtonDown", "PlayerWindow.OnLButtonDown")
	WindowRegisterCoreEventHandler(Window3DPortrait, "OnRButtonUp", "PlayerWindow.OnRButtonUp")

	-- Fifth step: define event handlers
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED,	"RVMOD_3DPortrait.OnAllModulesInitialized")
	RegisterEventHandler(SystemData.Events.LOADING_END,				"RVMOD_3DPortrait.OnLoadingEnd")
end

--------------------------------------------------------------
-- function Shutdown()
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.Shutdown()

	-- First step: destroy 3D portrait settings
	if DoesWindowExist(Window3DPortraitSettings) then
		DestroyWindow(Window3DPortraitSettings)
	end

	-- Second step: unregister events
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED,	"RVMOD_3DPortrait.OnAllModulesInitialized")
	UnregisterEventHandler(SystemData.Events.LOADING_END,				"RVMOD_3DPortrait.OnLoadingEnd")

	-- Third step: unregister 3D portrait window events
	WindowUnregisterCoreEventHandler(Window3DPortrait, "OnMouseOver")
	WindowUnregisterCoreEventHandler(Window3DPortrait, "OnMouseOverEnd")
	WindowUnregisterCoreEventHandler(Window3DPortrait, "OnLButtonDown")
	WindowUnregisterCoreEventHandler(Window3DPortrait, "OnRButtonUp")

	-- Fourth step: destroy all registered portaits
	for PortraitName, PortraitData in pairs(PortraitsList) do
		RVMOD_3DPortrait.API_Destroy3DPortrait(PortraitName)
	end

	-- Fifth step: destroy 3D portrait background
	if DoesWindowExist(Window3DPortraitBackground) then
		DestroyWindow(Window3DPortraitBackground)
	end

	-- Sixth step: destroy 3D portrait frame
	if DoesWindowExist(Window3DPortraitFrame) then
		DestroyWindow(Window3DPortraitFrame)
	end
end

--------------------------------------------------------------
-- function InitializeConfiguration()
-- Description: loads current configuration
--------------------------------------------------------------
function RVMOD_3DPortrait.InitializeConfiguration()

	-- First step: move default value to the CurrentConfiguration variable
	for k,v in pairs(RVMOD_3DPortrait.DefaultConfiguration) do
		if(RVMOD_3DPortrait.CurrentConfiguration[k]==nil) then
			RVMOD_3DPortrait.CurrentConfiguration[k]=v
		end
	end
end

--------------------------------------------------------------
-- function Initialize3DPortraitWindowSettings()
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.Initialize3DPortraitWindowSettings()

	-- First step: create main window
	CreateWindow(Window3DPortraitSettings, true)

	-- Second step: set on window texts
	LabelSetText(Window3DPortraitSettings.."LabelEnabled", L"Enabled")
	LabelSetTextColor(Window3DPortraitSettings.."LabelEnabled", 255, 255, 255)
end

--------------------------------------------------------------
-- function ShowModuleSettings()
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.Update3DPortraitWindowSettings()

	-- First step: update Enabled status
	ButtonSetPressedFlag(Window3DPortraitSettings.."CheckBoxEnabled", RVMOD_3DPortrait.CurrentConfiguration.Enabled)
end

--------------------------------------------------------------
-- function Update3DPortraitScenes()
-- Description: 
--------------------------------------------------------------
function RVMOD_3DPortrait.Update3DPortraitScenes()

	-- First step: calculate CanShow flag
	local CanShow = not WindowGetShowing("CharacterWindow") and RVMOD_3DPortrait.CurrentConfiguration.Enabled and GameData.Player.career.line > 0

	-- Second step: wipe all available 3D Portrait Scenes
	for PortraitName, PortraitData in pairs(PortraitsList) do

		-- : check for window
		if DoesWindowExist(PortraitName.."Scene") then

			-- : destroy window
			DestroyWindow(PortraitName.."Scene")
		end
	end

	-- Third step: check for the showing flag
	if CanShow then

		-- : get portrait template
		local PortraitTemplate = RVMOD_3DPortrait.Templates[GameData.Player.career.line][GameData.Player.gender]

		-- : create new scenes
		for PortraitName, PortraitData in pairs(PortraitsList) do

			-- : create scene
			CreateWindowFromTemplate(PortraitName.."Scene", PortraitTemplate, PortraitName)

			-- : anchor scene to the parent window
			WindowAddAnchor(PortraitName.."Scene", "topleft", PortraitName, "topleft", 0, 0)
			WindowAddAnchor(PortraitName.."Scene", "bottomright", PortraitName, "bottomright", 0, 0)
		end

	else

		-- : create original CharacterWindowScene with default settings
		DestroyWindow("CharacterWindowScene")
		CreateWindow("CharacterWindowScene", true)
		WindowSetParent("CharacterWindowScene", "CharacterWindowContents")
	end

	-- Fourth step: set flag to the 3D portrait background and frame
	WindowSetShowing("PlayerWindow3DPortraitBackground", CanShow)
	WindowSetShowing("PlayerWindow3DPortraitFrame", CanShow)

	-- Fifth step: set flag to the original portrait
	WindowSetShowing("PlayerWindowPortraitBackground", not CanShow)
	WindowSetShowing("PlayerWindowPortraitFrame", not CanShow)
	WindowSetShowing("PlayerWindowPortrait", not CanShow)
end

--------------------------------------------------------------
-- function OnAllModulesInitialized()
-- Description: 
--------------------------------------------------------------
function RVMOD_3DPortrait.OnAllModulesInitialized()

	-- First step: emulate OnLoadingEnd event
	RVMOD_3DPortrait.OnLoadingEnd()

	-- Final step: register in the RV Mods Manager
	-- Please note the folowing:
	-- 1. always do this ON SystemData.Events.ALL_MODULES_INITIALIZED event
	-- 2. you don't need to add RVMOD_Manager to the dependency list
	-- 3. the registration code should be same as below, with your own function parameters
	-- 4. for more information please follow by http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534
	if RVMOD_Manager then
		RVMOD_Manager.API_RegisterAddon("RVMOD_3DPortrait", RVMOD_3DPortrait, RVMOD_3DPortrait.OnRVManagerCallback)
	end
end

--------------------------------------------------------------
-- function OnRVManagerCallback
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.OnRVManagerCallback(Self, Event, EventData)

	if		Event == RVMOD_Manager.Events.NAME_REQUESTED then

		return RVName

	elseif	Event == RVMOD_Manager.Events.CREDITS_REQUESTED then

		return RVCredits

	elseif	Event == RVMOD_Manager.Events.LICENSE_REQUESTED then

		return RVLicense

	elseif	Event == RVMOD_Manager.Events.PROJECT_URL_REQUESTED then

		return RVProjectURL

	elseif	Event == RVMOD_Manager.Events.RECENT_UPDATES_REQUESTED then

		return RVRecentUpdates

	elseif	Event == RVMOD_Manager.Events.PARENT_WINDOW_UPDATED then

		if not DoesWindowExist(Window3DPortraitSettings) then
			RVMOD_3DPortrait.Initialize3DPortraitWindowSettings()
		end

		WindowSetParent(Window3DPortraitSettings, EventData.ParentWindow)
		WindowClearAnchors(Window3DPortraitSettings)
		WindowAddAnchor(Window3DPortraitSettings, "topleft", EventData.ParentWindow, "topleft", 0, 0)
		WindowAddAnchor(Window3DPortraitSettings, "bottomright", EventData.ParentWindow, "bottomright", 0, 0)

		RVMOD_3DPortrait.Update3DPortraitWindowSettings()

		return true

	end
end

--------------------------------------------------------------
-- function OnLoadingEnd()
-- Description: 
--------------------------------------------------------------
function RVMOD_3DPortrait.OnLoadingEnd()

	-- First step: update all scenes
	RVMOD_3DPortrait.Update3DPortraitScenes()
end

--------------------------------------------------------------
-- function OnShownCharacterWindow()
-- Description: 
--------------------------------------------------------------
function RVMOD_3DPortrait.OnShownCharacterWindow()

	-- First step: call original event
	hookCharacterWindowOnShown()

	-- Second step: update all scenes
	RVMOD_3DPortrait.Update3DPortraitScenes()
end

--------------------------------------------------------------
-- function OnHiddenCharacterWindow()
-- Description: 
--------------------------------------------------------------
function RVMOD_3DPortrait.OnHiddenCharacterWindow()

	-- First step: call original event
	hookCharacterWindowOnHidden()

	-- Second step: update all scenes
	RVMOD_3DPortrait.Update3DPortraitScenes()
end

--------------------------------------------------------------
-- function OnCheckBoxEnabled()
-- Description: 
--------------------------------------------------------------
function RVMOD_3DPortrait.OnCheckBoxEnabled()

	-- First step: get checkbox name
	local CheckBoxName = SystemData.MouseOverWindow.name

	-- Second step: invert flag status
	RVMOD_3DPortrait.CurrentConfiguration.Enabled = not RVMOD_3DPortrait.CurrentConfiguration.Enabled
	ButtonSetPressedFlag(CheckBoxName, RVMOD_3DPortrait.CurrentConfiguration.Enabled)

	-- Final step: update all scenes
	RVMOD_3DPortrait.Update3DPortraitScenes()
end


--------------------------------------------------------------------------------
--								RVMOD_3DPortrait API						  --
--------------------------------------------------------------------------------

--------------------------------------------------------------
-- function API_Does3DPortraitExist
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.API_Does3DPortraitExist(portraitName)

	-- Final step: return result
	return PortraitsList[portraitName] ~= nil
end

--------------------------------------------------------------
-- function API_Create3DPortrait
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.API_Create3DPortrait(portraitName, parentWindow)

	-- First step: set locals
	local parentWindow = parentWindow or "Root"

	-- Second step: check for dublicates
	if RVMOD_3DPortrait.API_Does3DPortraitExist(portraitName) then

		d("API_Create3DPortrait: "..portraitName.." is a duplicate!")
		return false
	end

	-- Third step: create container window
	if not CreateWindowFromTemplate(portraitName, Window3DPortraitContainerTemplate, parentWindow) then

		d("API_Create3DPortrait: portrait creation failed!")
		return false
	end

	-- Fourth step: save portrait default information
	PortraitsList[portraitName]	= {
		PortraitName	= portraitName,
		ParentWindow	= parentWindow,
		Width			= 100,
		Height			= 100,
	}

	-- Fifth step: set portrait default dimensions
	WindowSetDimensions(portraitName, PortraitsList[portraitName].Width, PortraitsList[portraitName].Height)

	-- Sixth step: update all scenes
	RVMOD_3DPortrait.Update3DPortraitScenes()

	-- Final step: return result
	return true
end

--------------------------------------------------------------
-- function API_Destroy3DPortrait
-- Description:
--------------------------------------------------------------
function RVMOD_3DPortrait.API_Destroy3DPortrait(portraitName)

	-- First step: check for portrait window
	if not RVMOD_3DPortrait.API_Does3DPortraitExist(portraitName) then

		d("API_Destroy3DPortrait: "..portraitName.." does not exist!")
		return false
	end

	-- Second step: destroy scene window
	if DoesWindowExist(portraitName.."Scene") then
		DestroyWindow(portraitName.."Scene")
	end

	-- Third step: destroy container window
	if DoesWindowExist(portraitName) then
		DestroyWindow(portraitName)
	end

	-- Fourth step: clear portrait information
	PortraitsList[portraitName]	= nil

	-- Final step: return result
	return true
end